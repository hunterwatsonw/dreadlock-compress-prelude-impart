# frozen_string_literal: true

require "json"

class CloudflareProxy
  def initialize(app)
    @app = app
  end

  def call(env)
    return @app.call(env) unless env["HTTP_CF_VISITOR"]

    # Remove protocol from all requests
    #HTTP Origin header -> request.base_url

    if (env["HTTP_X_FORWARDED_PROTO"] == "http" && !env["HTTP_ORIGIN"].nil? && env["HTTP_ORIGIN"].start_with?("https://"))
      env["HTTP_ORIGIN"] = env["HTTP_ORIGIN"].gsub("http://", "https://")
    end

    env["HTTP_X_FORWARDED_PROTO"] = JSON.parse(env["HTTP_CF_VISITOR"])["scheme"]
    @app.call(env)
  end
end
