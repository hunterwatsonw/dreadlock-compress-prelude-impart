class AskBookController < ActionController::Base
  DATABASE_PATH = "db/development.sqlite3"

  # Disable CSRF protection for the index action
  # TODO: This is a hack. Fix it.
  #protect_from_forgery except: [:index, :filename]

  def initialize
    @is_built = false
    @db_built = false

    @filename = "storage/documents/mushroom-info.txt"

    build()
  end

  # GET /filename
  def filename
    pretty_name = @filename.split("/").last
    render json: { filename: pretty_name }, status: 200
  end

  # Build the models and providers
  def build
    if @is_built
      return
    end

    puts "~~~~~~ [AskBookController] - Building..."

    #filename = "storage/documents/2454180-2.pdf"

    # 1. Create a new document provider
    # TODO: Make @filename a parameter
    #DONE: Detect file type and use the appropriate provider

    #documentTextProvider = PDFDocumentTextProvider.new(filename)
    #documentTextProvider = TXTDocumentTextProvider.new(@filename)
    documentTextProvider = DocumentTextProvider.get_provider_for_file(@filename) # Detects file type, wow

    # 2. Create a new document model
    sqliteDocumentTextModel = SQLiteDocumentTextModel.new(DATABASE_PATH, documentTextProvider)
    # 2.1 conditionally generate the database
    if !@db_built
      sqliteDocumentTextModel.build()
      @db_built = true
    end

    # 3. Create a new OpenAI provider with a cache provider
    @openAICacheProvider = SQLiteOpenAICacheProvider.new(DATABASE_PATH)
    @openAIProvider = OpenAIProvider.new(@openAICacheProvider)

    # 4. Create a new document embeddings model
    @documentEmbeddingsModel = SQLiteDocumentEmbeddingsModel.new(DATABASE_PATH, sqliteDocumentTextModel, @openAIProvider)

    # 5. Create a new document search model
    @documentSearchModel = DocumentSearchModel.new(@documentEmbeddingsModel)

    # Done! ^_^
    puts "~~~~~~ [AskBookController] - Built."
    @is_built = true
  end

  # Git Command to keep my code while making it a repo (pull)
  # PS C:\Users\JohnDoe\rails\book_reactapp\lib\EmbeddingPromptedSearch> git clone https://github.com/hwvs/Embedding-Prompted-Search-Ruby.git ./
  #fatal: destination path '.' already exists and is not an empty directory.

  # GET /ask_book
  def index

    # Check for params[:question, :refresh (optional)]
    if params[:question].nil?
      render json: { success: false, error: "Missing question parameter" }, status: 400
      return
    end

    # Check for params[:refresh]
    refresh = false
    if !params[:refresh].nil?
      refresh = params[:refresh] == true || params[:refresh] == "true"
    end

    # enforce question is a string
    question = params[:question].to_s

    # enforce length of question is > 0
    if question.length == 0
      render json: { success: false, error: "Question parameter cannot be empty" }, status: 400
      return
    end

    # enforce length of question is < 1024
    if question.length > 1024
      render json: { success: false, error: "Question parameter cannot be longer than 1024 characters" }, status: 400
      return
    end

    begin
      answer = nil
      if !refresh
        # TODO: Check memcached for tempoarily cached answer
      end

      # fallback to method call if not found in cache
      if answer.nil?
        answer = ask(question, refresh)
      end

      if answer.nil?
        render json: { success: false, error: "No answer found" }, status: 503
        return
      else
        render json: { success: true, answer: answer }
        # TODO: Cache answer in memcached
      end
    rescue => exception
      render json: { success: false, error: exception.message }, status: 503
    end
  end

  # Create a new answer for the given question with multi-shot examples
  private def build_prompt(question, tone)

    # Gather the first 1024 characters (or more if the first block is >1024char) of the document that match the question
    prompt_info_block = ""
    all_matches = @documentSearchModel.SearchTextBlocks(question)
    # Grab from all_matches until prompt_info_block is 1024 characters, but always include at least the entire first match
    all_matches.each do |match|
      if prompt_info_block.length > 0 && prompt_info_block.length + match.length > 1024
        break
      end
      prompt_info_block += "\n" + match["block"]
    end

    # Warn if >4096 characters, that means the text block splitting may have failed. That shouldn't be possible unless modified.
    # If this happens look at the text block splitting code in DocumentTextModel.rb
    if prompt_info_block.length > 4096
      puts "~~~~~~ [AskBookController] - Warning: prompt_info_block is >4096 characters"
    end

    # ===============================
    # ========  BUILD PROMPT ========
    # ===============================

    # [Prompt] The style/tone of the response
    if tone.nil?
      prompt = "All answers should be for a general audience. Answer as if it is your own opinion. "
      prompt += "You are encouraged to use humor, sarcasm, and personal anecdotes. "
      prompt += "Please do not use overly formal language."
      prompt += "\n"
    else
      prompt = tone + "\n"
    end

    # [Prompt] Text from the document
    prompt += "If there is a specific mushroom in the document, use the common name. "
    prompt += "If an example in the document matches the question, mention it. "
    prompt += "Use this reference material to answer questions:\n\n```\n#{prompt_info_block}\n```\n\n---\n"

    # [Prompt] Multi-shot Examples
    prompt += "Question: What is a mushroom?\nAnswer: A mushroom is any type of fungus! I think they're pretty neat.\n"
    prompt += "Question: Are mushrooms dangerous?\nAnswer: Don't eat them without an expert identification! However, even poisonous mushrooms are perfectly safe to handle.\n"

    # [Prompt] The actual question
    prompt += "Question: #{question}\nAnswer:"

    return prompt
  end

  # Ask the question and return the answer as a string
  #
  # @param question [String] The question to ask
  # @param refresh [Boolean] Whether to refresh the cached answer
  # @param tone [String] The tone of the answer
  # @return [String] The answer
  private def ask(question, refresh, tone = nil) # TODO: Implement tone parameter (based on document? or user input?) This is a stretch goal (whatever that means)
    if !@is_built
      build()
    end

    puts "~~~~~~ [AskBookController] - Asking question: #{question}, refresh: #{refresh}"

    prompt = build_prompt(question, tone)

    #  def get_completion_for_text(text, model = nil, stop = nil, max_tokens = 512, temperature = 0.7, top_p = 1, frequency_penalty = 0, presence_penalty = 0.0)

    annoying_moderation_words = ["However, ", "It is important ", "It's important"] #ItS ImPoRtAnT tO ReMeMbEr ThAt MuShRoOmS aRe DaNgErOuS
    answer = @openAIProvider.get_completion_for_text(prompt, nil, stop = ["\n"] + annoying_moderation_words, bypass_cache = refresh, max_tokens = 512)

    return answer
  end
end
