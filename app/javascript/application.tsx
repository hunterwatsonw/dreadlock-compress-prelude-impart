import * as React from 'react'
import * as ReactDOM from 'react-dom/client'
import { createPopper } from '@popperjs/core';
import type { StrictModifiers } from '@popperjs/core';

// Random BS
const feelingLuckyQuestions = [
  'Who is the current author of this book?',
  'What are mushrooms?',
  'Who invented the mushroom?',
  'What kind of mushroom does Mario eat?',
  'Is it possible for mushrooms to lower LDL cholesterol without affecting HDL?',
]
const answerCache = {} // Cache answers locally

// POST /ask_book?question=What is the current author of this bool?
// 200 - {"success":true,"answer":" The current value of the page is 0."}

const getCSRFToken = () => {
  const csrfToken = document.querySelector<HTMLMetaElement>('[name=csrf-token]')!.content
  const csrfParam = document.querySelector<HTMLMetaElement>('[name=csrf-param]')!.content
  return { csrfToken, csrfParam }
}


const askBook = async (question: string, refresh: boolean) => {
  // Rails csrf_meta_tags fix
  const headers = {
    'Content-Type': 'application/json',
  }


  const { csrfToken, csrfParam } = getCSRFToken()
  const response = await fetch('/ask_book', {
    method: 'POST',
    headers: headers,
    body: JSON.stringify({ question: question, refresh: refresh, [csrfParam]: csrfToken })
  })
  if (!response.ok) {
    throw new Error('Response not 200_OK - ' + response.status + ' ' + response.statusText + ' ' + (await response.json()).error)
  }
  const data = await response.json()
  if(!data.success) {
    throw new Error('Response not success - ' + data.error)
  }
  return data
}
const App = () => {
  const [question, setQuestion] = React.useState('What is your favorite cosmopolitan mushroom, native to conifer, and why?')
  const [answer, setAnswer] = React.useState('')
  const [error, setError] = React.useState('')
  const [loading, setLoading] = React.useState(false)
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>, refresh:boolean = false) => {
    if (e) {
      e.preventDefault()
      e.stopPropagation()
    }

    if (question.length < 1) {
      setError('Question must be at least 1 character long')
      return false
    }

    await doAskBook(question, refresh)

    return false
  }


  const doAskBook = async (question, refresh) => {
    
    setLoading(true)
    setError('')
    setAnswer('')
    try {
       // If not regenerating answer, check cache, if exists, use it
      if(!refresh && answerCache[question]) {
        setAnswer(answerCache[question]) // Cache hit
        
        return
      }
      else {

        // Get the answer from the API
        const data = await askBook(question, refresh)
        setAnswer(data.answer) // Update answer

        // Cache answer locally
        answerCache[question] = data.answer
      }

    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  const doFeelingLucky = async (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault()
    e.stopPropagation()

    // Get a random question which is not the current question
    let newQuestion = question
    while (newQuestion === question) {
      newQuestion = feelingLuckyQuestions[Math.floor(Math.random() * feelingLuckyQuestions.length)]
    }
    setQuestion(newQuestion)
    await doAskBook(newQuestion, false) // Use newQuestion, due to react state update delay
    return false
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col row-question">
          <form onSubmit={handleSubmit}>
            <div className="row">
              <label className="col-sm-2 col-form-label">Question:</label>
              <textarea className="form-control" rows={3} value={question} onChange={e => setQuestion(e.target.value)} maxLength={512} />
            </div>
            <div className="row row-buttons">
                <input type="submit" className="btn btn-primary" value="Ask" disabled={loading} />
                <input type="button" className="btn btn-secondary" value="I&apos;m Feeling Lucky" onClick={async e => { e.preventDefault(); await doFeelingLucky(e); return false; } } disabled={loading} />
            </div>
          </form>
          <div className="row row-answer">
            {loading && <div><strong>[ Distilling... ]</strong></div>}
            {error && <div><strong>Error:</strong> {error}</div>}
            {answer && <div><strong>Answer:</strong> {answer}
              <div className="row row-new-answer">
                <button type="button" className="btn btn-primary" onClick={e => { handleSubmit(e, true); } }>
                  Give me a different answer
                </button>
              </div>
            </div>}
          </div>
        </div>
      </div>
    </div>
  )
}



// Update #current_document with filename
const getFilename = async () => {
  const { csrfToken, csrfParam } = getCSRFToken()

  // POST
  const headers = {
    'Content-Type': 'application/json',
    [csrfParam]: csrfToken
  }
  
  const body = JSON.stringify({ [csrfParam]: csrfToken })
  const response = await fetch('/filename', {method: 'POST', body: body, headers: headers})
  if (!response.ok) {
    throw new Error('Response not 200_OK - ' + response.status + ' ' + response.statusText + ' ' + (await response.json()).error)
  }
  const data = await response.json()
  return data
}
const updateFilename = async () => {
  try {
    const data = await getFilename()
    document.getElementById('current_document')!.innerHTML = data.filename
  } catch (err) {
    console.error(err)
  }
}
updateFilename()



document.addEventListener('DOMContentLoaded', () => {
  const rootEl = document.getElementById('app')
  ReactDOM.createRoot(rootEl).render(<App />)
})