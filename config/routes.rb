Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  # Defines the root path route ("/")
  root "application#index"

  get "/ask_book" => "ask_book#index"
  post "/ask_book" => "ask_book#index"
  post "/filename" => "ask_book#filename"
end
